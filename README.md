# Golang Quiz App

This is my practice project !!

---

A quiz app which fetch questions and answers from a `.csv` file.

# To Use

Clone with shh :

````bash
git clone git@gitlab.com:Skaryus/quiz_app.git
````

Clone with http :

```bash
git clone https://gitlab.com/Skaryus/quiz_app.git
```
After downloading has been finished, go project directory then type :

```gitbash
go run main.go
```
